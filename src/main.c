/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ecollot <ecollot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/01 18:05:48 by ecollot           #+#    #+#             */
/*   Updated: 2014/01/10 18:15:04 by ecollot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>
#include <signal.h>
/*
void		ft_recup(int i)
{
	t_caps 		*caps;
	t_termios	*term;

	(void)i;
	caps = (t_caps *)ft_get_caps();
	term = caps->term;
	term_raw(term);
	tputs(tgetstr("ti", NULL), 1, my_putchar);
	re_draw(caps->head);
}

void		ft_stop(int i)
{
//	char		susp[2];
	t_caps		*caps;
	t_termios	*term;

	(void)i;
	caps = (t_caps *)ft_get_caps();
	term = caps->term;
	tputs(tgetstr("te", NULL), 1, my_putchar);
	term_default(caps->term);
//	susp[0] = term->c_cc[VSUSP];
//	susp[1] = '\0';
//	signal(SIGTSTP, SIG_DFL);
//	ioctl(0, TIOCSTI, susp);
}

void		ft_kill(int i)
{
	t_caps	*caps;

	caps = (t_caps *)ft_get_caps();
	ft_exit(caps->head, caps->term, 1);
	(void)i;
}
*/
void	ft_resize(int i)
{
	(void)i;
	re_draw((t_caps *)ft_get_caps());
}

int		main(int ac, char **av)
{
	t_termios		term;
	t_caps			*caps;
	char			read_char[5] = {0};
	unsigned int	buf;
	int				ret;

	signal(SIGWINCH, ft_resize);
//	signal(SIGCONT, ft_recup);
//	signal(SIGKILL, ft_kill);
//	signal(SIGQUIT, ft_kill);
//	signal(SIGINT, ft_kill);
//	signal(SIGTSTP, ft_stop);

	caps = ft_parsor(ac, av + 1);
	term_init();
	term_raw(&term);
	caps->term = &term;
	tputs(tgetstr("ti", NULL), 1, my_putchar);
	re_draw(caps);
	while (1)
	{
		tputs(tgetstr("ks", NULL), 1, my_putchar);
		if ((ret = read(0, read_char, 4)) == -1)
		{
			tputs(tgetstr("te", NULL), 1, my_putchar);
			term_default(&term);
			ft_exit(caps, 1);
		}
		buf = *(unsigned int *)read_char;
		tputs(tgetstr("ke", NULL), 1, my_putchar);
		term_event(buf, caps);
		read_char[0] = 0;
		read_char[1] = 0;
		read_char[2] = 0;
		read_char[3] = 0;
		read_char[4] = 0;
	}
	tputs(tgetstr("te", NULL), 1, my_putchar);
	term_default(&term);
	return (0);
}
